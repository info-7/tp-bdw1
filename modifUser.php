<? include_once "functions/include.php";

define("ERR_LOG_EXISTS", "ERR_LOG_EXISTS");
define("ERR_PASSWDS_NOT_IDENTICAL", "ERR_PASSWDS_NOT_IDENTICAL");
define("ERR_PASSWD_NOT_VALID", "ERR_PASSWD_NOT_VALID");

function getErrlog()
{
    $conn = getLinkToDb();
    $err = "";
    setWatchDog(!isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"]), "index.php");

    if ($_SERVER["REQUEST_METHOD"] != "POST");
    else if($_POST["cancel"] == "yes") redirectUserTo("index.php");
    else if (!isGoodLogin($conn, $_SESSION["pseudo"], $_POST["oldPasswd"])) {
        $err .= ERR_PASSWD_NOT_VALID;
    } else if ($_POST["passwd1"] != $_POST["passwd2"]) {
        $err .= ERR_PASSWDS_NOT_IDENTICAL;
    } else {

        $newMdp = NO_CHANGE;

        if (!empty($_POST["passwd1"])) $newMdp = md5($_POST["passwd1"]);
        updateDatas($conn, utilisateurs_db, ["idU", $_SESSION["pseudo"], NO_CHANGE], ["passwdHashed", ALL, $newMdp]);

        redirectUserTo("index.php");
    }
    $conn->close();
    if(!empty($err)) return ("<label class=\"p-3 mb-3 bg-danger text-light\">" . $err . "</label>");
}

?>

<?
session_start();
?>

<!doctype html>
<html>

<head>
    <meta charset="utf-8">

    <? includeScriptCss(); ?>
</head>

<body class="container fill">

    <main class="row h-100 justify-content-center">
        <form class="col-xs-12 col-md-12 col-lg-4 col-sm-12 col-xl-4 my-auto" method="POST">
            <h1 class="h3 mb-3 fw-normal justify-content-center">Changement des informations</h1>
            <div class="form-floating">
                <input class="form-control" name="oldPasswd" id="oldPasswd" placeholder="Password">
                <label for="oldPasswd">Mot de passe actuel</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" name="passwd1" id="passwd1" placeholder="Password">
                <label for="passwd1">Nouveau mot de passe</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" name="passwd2" id="passwd2" placeholder="Password">
                <label for="passwd2">Confirmation du mot de passe</label>
            </div>
            <div>
                <br><button class="w-100 btn btn-lg btn-primary" type="submit">Confirmation</button>
            </div>
            <div>
                <br><button class="w-100 btn btn-lg btn-primary" name="cancel" value="yes" type="submit">Abandon</button>
            </div>
            <div class="mt-3 d-flex justify-content-center">
                <? echo getErrLog(); ?>
            </div>


        </form>
    </main>

</body>

</html>