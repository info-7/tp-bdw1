<?php
include_once "functions/include.php"
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <?
    session_start();
    echo $_GET["recherche"];
    echo $_GET["categorie"];
    includeScriptCss();
    handleDisconnect();
    displayNavBar();
    ?>



</head>

<body>


    <div class="container" style="height:90vh; float :none;">
        <div class="row h-100 justify-content-center">

            <?php
            $conn = getLinkToDb();

            // Vérifier si le formulaire a été soumis
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $images = getDatasLike($conn, "photo P JOIN categorie C ON P.idCat = C.idCat", ["nomFich", $_POST["image"]]); //récupérer la photo et sa catégorie
                foreach ($images as $val) {
                    echo '
                    <div class="col-md-12 col-lg-6 col-xl-6  my-auto">
                        <img src="pictures/' . $val["nomFich"] . '" class="rounded picture">
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6  my-auto">
                        <h1> ' . $val["nomFich"] . ' </h1>
                        <br/>
                        <p><h2> Description : </h2>' . $val["description"] . ' </p>
                        <br/>
                        <form action="index.php" method="post">
                        <input id="idCat" name="idCat" type="hidden" value="' . $val["idCat"] . '">
                        <button class="btn btn-link" type="submit"> Catégorie : ' . $val["nomCat"] . ' </button>
                        </form>
                        ';
                    $admin = "0";
                    $user = getUsers($conn, $_SESSION["pseudo"]);
                    if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
                        foreach ($user as $use) {
                            $admin = $use["admin"];
                        }
                    }
                    if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"]) and ($_SESSION["pseudo"] == $val["idU"] or $admin == "1")) { //ajouter la modif par admin
                        if ($val["estCachee"] == 1) {
                            echo '
                                <form action="image.php" method="post" style="margin-left : 10px">
                                <input id="image" name="image" type="hidden" value="' . $val["nomFich"] . '">
                                <input id="estCachee" name="estCachee" type="hidden" value="0">
                                <button class="btn btn-warning" type="submit" style="float:left; margin-left : 10px"> Publier image </button></form>';
                        } else {
                            echo '
                                <form action="image.php" method="post" style="margin-left : 10px">
                                <input id="image" name="image" type="hidden" value="' . $val["nomFich"] . '">
                                <input id="estCachee" name="estCachee" type="hidden" value="1">
                                <button class="btn btn-warning" type="submit" style="float:left; margin-left : 10px"> Cacher image </button></form>';
                        }
                        echo '
                                <form action="image.php" method="post" style="float:left; margin-left : 10px">
                                <input id="image" name="image" type="hidden" value="' . $val["nomFich"] . '">
                                <input id="delete" name="delete" type="hidden" value="ok">
                                <button class="btn btn-danger" type="submit" style="float:left; margin-left : 10px"> Supprimer image </button></form>';

                        if ($_POST["estCachee"] == "0")
                            udpatePicture($conn, $val["nomFich"], NO_CHANGE, NO_CHANGE, "0");
                        else if($_POST["estCachee"] == "1")
                            udpatePicture($conn, $val["nomFich"], NO_CHANGE, NO_CHANGE, "1");

                        if ($_POST["delete"] == "ok") {
                            deletePicture($conn, $val["nomFich"]);
                            echo '<script>window.location.replace("index.php"); </script>';
                        }
                    }


                    echo '</div>';
                }
            } else {
                echo '<script>window.location.replace("index.php"); </script>';
            }
            ?>

        </div>
    </div>


</body>