<?php
//Inclusion des ressources
include_once "functions/include.php";

function printStat($name, $data)
{
    echo "<div class=\"row\">
                    <div class=\"col\" align=\"center\">
                        <p>" . $name . "</p>
                    </div>
                    <div class=\"col\" align=\"center\">" .
        $data
        . "</div>
                    </row>
                </div>";
}
?>

<!DOCTYPE html>
<html>

<head>

    <?
    session_start(); /// Démarrage de la session

    includeScriptCss(); /// Inclusion des feuilles de styles

    handleDisconnect();
    displayNavBar();
    ?>
    <?php
    setWatchDog(!isIdIn(getLinkToDb(), utilisateurs_db, "idU", $_SESSION["pseudo"], ["admin", "1"]), "index.php");
    ?>
</head>

<body>

    <div class="container fill" style="float :none;">

        <main class="row h-100 justify-content-center">
            <div class="col-xs-12 col-md-12 col-lg-7 col-sm-12 col-xl-7 my-auto">
                <h3 class="h3 mb-3 fw-normal" align="center">Statistiques du site web</h3>

                <?
                printStat("Nombre d'utilisateurs", countRowIn(getLinkToDb(), utilisateurs_db));
                printStat("Nombre d'administrateurs", countRowIn(getLinkToDb(), utilisateurs_db, ["admin", "1"]));
                ?>
                <hr class="solid">
                <?
                printStat("Nombre de photos", countRowIn(getLinkToDb(), photo_db));
                printStat("Taille des photos sur le server", getSizeOfFolder(picturesFolder));
                printStat("Nombre de photos visibles", countRowIn(getLinkToDb(), photo_db, ["estCachee", "0"]));
                printStat("Nombre de catégories", countRowIn(getLinkToDb(), categorie_db));

                ?>
            </div>

</body>