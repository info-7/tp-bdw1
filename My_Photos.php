<?php
//Inclusion des ressources
include_once "functions/include.php";
?>

<!DOCTYPE html>
<html>

<head>

    <?
    session_start(); /// Démarrage de la session
    echo $_GET["recherche"];
    echo $_GET["categorie"];

    includeScriptCss(); /// Inclusion des feuilles de styles

    handleDisconnect();
    displayNavBar();
    ?>

</head>

<body>


    <div class="container-fluid">
        <div class="row justify-content-center">


            <?php
            $conn = getLinkToDb();
            if (!isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"]))
                echo '<script>window.location.replace("index.php"); </script>';

            $admin = "0";
            $nbImages = 0;
            $user = getUsers($conn, $_SESSION["pseudo"]);
            if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
                foreach ($user as $use) {
                    $admin = $use["admin"];
                }
            }

            $categorie = ALL;
            if ($_SERVER["REQUEST_METHOD"] == "POST")
                $categorie = $_POST["idcat"];

            if ($admin == "0") {

                if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
                    $liste = getPicture($conn, $_SESSION["pseudo"], $categorie);
                    if (is_array($liste) || is_object($liste)) {
                        foreach ($liste as $val) {
                            $table .= ' <div class="col-md-12 col-lg-6 col-xl-4 " style="marin-top:30px; margin-bottom:30px"><form action="image.php" method="post" class="img" style="height:100%">
                        <input id="image" name="image" type="hidden" value="' . $val["nomFich"] . '">
                        <input name="submit" src="pictures/' . $val["nomFich"] . '" type="image" class="img-thumbnail cover picture" style="height:100%; width:100%; display:block; object-fit: cover; " data-holder-renderer="true"  Value="" ></form></div>' . "\n";
                        $nbImages ++;
                        }
                    }
                }
            }else if($admin == "1"){
                if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
                    $liste = getPicture($conn, ALL, $categorie);
                    if (is_array($liste) || is_object($liste)) {
                        foreach ($liste as $val) {
                            $table .= ' <div class="col-md-12 col-lg-6 col-xl-4 " style="marin-top:30px; margin-bottom:30px"><form action="image.php" method="post" class="img" style="height:100%">
                        <input id="image" name="image" type="hidden" value="' . $val["nomFich"] . '">
                        <input name="submit" src="pictures/' . $val["nomFich"] . '" type="image" class="img-thumbnail cover picture" style="height:100%; width:100%; display:block; object-fit: cover; " data-holder-renderer="true"  Value="" ></form></div>' . "\n";
                        $nbImages ++;
                        }
                        
                    }
                }
            }
            echo '<div class="alert alert-success " role="alert" style="text-align: center">'
            .$nbImages .' image(s) sélectionnée(s)
        </div>';
        echo $table;


            ?>

        </div>
    </div>


</body>

</html>