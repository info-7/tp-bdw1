<?php
include_once "functions/include.php";

function getErrLog()
{
  if (!empty($_POST["pseudo"])) {
    $conn = getLinkToDb();
    if (isGoodLogin($conn, $_POST["pseudo"], $_POST["passwd"])) {
      
      $_SESSION["pseudo"] = $_POST["pseudo"];
      $_SESSION["dateCo"] = date('D M d Y H:i:s O');
      redirectUserTo("index.php");
    } else return "<label class=\"p-3 mb-3 bg-danger text-light\">Echec d'authentification</label>";
    $conn->close();
  }
}
?>

<?
session_start();
?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/custom.css" rel="stylesheet">
</head>

<body class="text-center">

  <div class="container fill" style="float :none;">

    <main class="row h-100 justify-content-center">
      <form class="col-xs-12 col-md-12 col-lg-4 col-sm-12 col-xl-4 my-auto" method="POST">
        <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

        <div class="form-floating">
          <input class="form-control" name="pseudo" id="pseudo" placeholder="name@example.com">
          <label for="floatingInput">Pseudo</label>
        </div>
        <div class="form-floating">
          <input type="password" class="form-control" name="passwd" id="passwd" placeholder="Password">
          <label for="floatingPassword">Mot de passe</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
        <div class="d-flex justify-content-center">
          <a href="Sign_up.php"><label cursor="pointer">Toujours pas membre ?</label></a>
        </div>
        <div>
        <? echo getErrLog(); ?>
        </div>
      </form>
      
  </div>

</body>

</html>