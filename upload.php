<?php
include_once "functions/include.php"
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <?
    session_start();
    includeScriptCss();
    handleDisconnect();
    displayNavBar();
    ?>

    <?php
    setWatchDog(!isIdIn(getLinkToDb(), utilisateurs_db, "idU", $_SESSION["pseudo"]),  "Sign_In.php");
    ?>

</head>

<body>



    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-6 col-xl-4">

                <form action="upload.php" method="post" enctype="multipart/form-data">
                    <h2>Upload Fichier</h2>
                    <label for="fileUpload">Fichier:</label>
                    <input type="file" name="photo" id="fileUpload" class="form-control-file" accept="image/png, image/jpeg, image/gif, image/jpg">



                    <label class="form-check-label" for="flexCheckChecked">
                        Image publique ?
                        <input class="form-check-input" type="checkbox" value="1" name="checkPublic" id="checkPublic" checked>
                        (option changeable ultérieurement)
                    </label>
                    <select name="idcat" class="form-select" id="idcat">
                        <option value = "nonSelect">--Sélectionner une catégorie--</option>
                        <? $categories = getCategorie(getLinkToDb());
                        foreach ($categories as $row) {
                            echo "<option value=\"" . $row["idCat"] . "\">" . $row["nomCat"] . "</option>";
                        }
                        ?>
                    </select>
                    <label for="desc">Description</label>
                    <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                    <p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 100 ko.</p>

                    <input type="submit" class="form-control" name="submit" value="Upload">

                </form>


                <?php
                // Vérifier si le formulaire a été soumis
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // Vérifie si le fichier a été uploadé sans erreur.
                    if (isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0) {
                        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                        $filename = $_FILES["photo"]["name"];
                        $filetype = $_FILES["photo"]["type"];
                        $filesize = $_FILES["photo"]["size"];

                        // Vérifie l'extension du fichier
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if (!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.");

                        // Vérifie la taille du fichier - 100ko maximum
                        $maxsize = 1024  * 100;
                        if ($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");
                        $conn = getLinkToDb();
                        // Vérifie si la description
                        if (!(empty($_POST["description"]) or ($_POST["idcat"]=="nonSelect"))) {
                            // Vérifie si le fichier existe avant de le télécharger.
                            if (file_exists("upload/" . $_FILES["photo"]["name"])) {
                                echo $_FILES["photo"]["name"] . " existe déjà.";
                            } else  if (addPicture($conn, $filename, $_SESSION["pseudo"], $_POST["idcat"], $_POST["description"], ($_POST["checkPublic"] == "1") ? "0" : "1")) {
                                $liste = getDatasLike($conn, photo_db, ["nomFich", $filename] );
                                $idP = "0";
                                    foreach ($liste as $val) {
                                        
                
                                    
                                    $nom = "DSC".$val["idPhoto"].".".$ext;
                                    echo $nom;
                                    updateDatas($conn, photo_db, ["nomFich", $filename, $nom]);
                                move_uploaded_file($_FILES["photo"]["tmp_name"], "pictures/".$nom);
                                echo "Votre fichier a été téléchargé avec succès.";
                                echo '<form name="redirect" action="image.php" method="post" class="img" style="height:100%">
                                <input id="image" name="image" type="hidden" value="' . $nom . '"></form>';
                                echo '<script>document.forms["redirect"].submit();</script>';
                            
                            }
                            } else
                                echo "Error: Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer.";
                        } else {
                            echo "Error: La description est vide ou aucune catégorie n'a été sélectionnée";
                        }
                    } else {
                        echo "Error: La photo n'a pas été correctement sélectionnée";
                    }
                }
                ?>
            </div>
        </div>
    </div>


</body>