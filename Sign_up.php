<? include_once "functions/include.php";

define("ERR_LOG_EXISTS", "ERR_LOG_EXISTS");
define("ERR_PASSWDS_NOT_IDENTICAL", "ERR_PASSWDS_NOT_IDENTICAL");

function getErrlog()
{
  if (!empty($_POST["pseudo"])) {
    return $_SESSION["ERR_SIGN_UP"];
  }
}

?>

<?
session_start();

if (!empty($_POST["pseudo"])) {
  $conn = getLinkToDb();
  if ($_POST["passwd1"] != $_POST["passwd2"]) $_SESSION["ERR_SIGN_UP"] = "<label class=\"p-3 mb-3 bg-danger text-light\">" . ERR_PASSWDS_NOT_IDENTICAL . "</label>";
  else if (isIdIn($conn, utilisateurs_db, "idU", $_POST["pseudo"])) $_SESSION["ERR_SIGN_UP"] = "<label class=\"p-3 mb-3 bg-danger text-light\">" . ERR_LOG_EXISTS . "</label>";
  else {
    $_SESSION["pseudo"] = $_POST["pseudo"];
    $_SESSION["dateCo"] = date('D M d Y H:i:s O');
    addUser($conn, $_POST["pseudo"], "0", $_POST["passwd1"]);
    header("Location: index.php");
    return END;
  }
  $conn->close();
}

?>

<!doctype html>
<html>

<head>
  <meta charset="utf-8">

  <? includeScriptCss(); ?>
</head>

<body class="container fill">

  <main class="row h-100 justify-content-center">
    <form class="col-xs-12 col-md-12 col-lg-4 col-sm-12 col-xl-4 my-auto" method="POST">
      <h1 class="h3 mb-3 fw-normal justify-content-center">Please sign in</h1>

      <div class="form-floating">
        <input class="form-control" name="pseudo" id="pseudo" placeholder="name@example.com">
        <label for="pseudo">Pseudo</label>
      </div>
      <div class="form-floating">
        <input type="password" class="form-control" name="passwd1" id="passwd1" placeholder="Password">
        <label for="passwd1">Mot de passe</label>
      </div>
      <div class="form-floating">
        <input type="password" class="form-control" name="passwd2" id="passwd2" placeholder="Password">
        <label for="passwd2">Confirmation du mot de passe</label>
      </div>
      <div>
      <br><button class="w-100 btn btn-lg btn-primary" type="submit">Confirmation</button>
      </div>
      <div class="d-flex justify-content-center">
        <a href="Sign_In.php"><label cursor="pointer">Déjà membre ?</label></a>
      </div>
      <div class="mt-3 d-flex justify-content-center">
        <?echo getErrLog(); ?>
      </div>

      
    </form>
  </main>

</body>

</html>