<?php
    define('photo_db', "photo");
    define('photo_vs', "photo_visible");
    include_once("bd.php");

    /**
     * @brief Ajoute un image
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     * idU : Identifiant de l'utilisateur dont l'image émane
     * idCat : Identifiant de la catégorie de la photo
     * description : Description de l'image
     * estCachee : Booléeb difinissant si l'image est visible ou non
     */
    function addPicture($conn, $nomFich, $idU, $idCat, $description = "NULL",$estCachee = "0"){
        return addData($conn, photo_db, ["nomFich", $nomFich], ["description", $description], ["idCat", $idCat], ["estCachee", $estCachee], ["idU", $idU]);
    }

        /**
     * @brief Supprime un image
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     */
    function deletePicture($conn, $nomFich){
        unlink("pictures/".$nomFich);
        return deleteDatas($conn, photo_db, ["nomFich", $nomFich]);
    }

        /**
     * @brief Retourne les images correspondant à la recherche
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     * admin : Renseigne si l'image est admin ou non
     * passwd : Mot de passe
     */
    function getPicture($conn, $idU = ALL, $idCat = ALL, $estCachee = ALL, $description = ALL){
        return getDatasLike($conn, photo_db, ["idU", $idU], ["idCat", $idCat], ["estCachee", $estCachee], ["description", $description]);
    }

        /**
     * @brief Met à jour les images
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     * admin : Renseigne si l'image est admin ou non
     * passwd : Mot de passe
     */
    function udpatePicture($conn, $nomFich, $description = NO_CHANGE, $idCat = NO_CHANGE, $estCachee = NO_CHANGE){
        return updateDatas($conn,photo_db, ["nomFich", $nomFich, NO_CHANGE], ["description", ALL, $description], ["idCat", ALL, $idCat], ["estCachee", ALL, $estCachee]);
    }
