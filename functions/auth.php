<?php
    define('utilisateurs_db', "utilisateurs");
    include_once 'bd.php';
    
    /**
     * @brief Ajoute un utilisateur
     * 
     * @Param conn : Connexion à la DB
     * idU : Identifiant utilisateur
     * admin : Renseigne si l'utilisateur est admin ou non
     * passwd : Mot de passe
     */
    function addUser($conn, $idU, $admin, $passwd){
        return addData($conn, utilisateurs_db, ["idU", $idU], ["admin", $admin], ["passwdHashed", md5($passwd)]);
    }

    /**
     * @brief Supprime un utilisateur
     * 
     * @Param conn : Connexion à la DB
     * idU : Identifiant utilisateur
     */
    function deleteUser($conn, $idU){
        return deleteDatas($conn, utilisateurs_db, ["idU", $idU]);
    }
    
    /**
     * @brief Retourne les utilisateurs correspondant à la recherche
     * 
     * @Param conn : Connexion à la DB
     * idU : Identifiant utilisateur
     * admin : Renseigne si l'utilisateur est admin ou non
     * passwd : Mot de passe
     */
    function getUsers($conn, $idU = ALL, $admin = ALL, $passwd = ALL){
        return getDatasLike($conn, utilisateurs_db, ["idU", $idU], ["admin", $admin], ["passwdHashed", $passwd]);
    }

    /**
     * @brief Met à jour les utilisateurs
     * 
     * @Param conn : Connexion à la DB
     * idU : Identifiant utilisateur
     * admin : Renseigne si l'utilisateur est admin ou non
     * passwd : Mot de passe
     */
    function udpateUser($conn, $idU, $admin = NO_CHANGE, $passwd = NO_CHANGE ){
        return updateDatas($conn, utilisateurs_db, ["idU", $idU, NO_CHANGE], ["admin", ALL, $admin], ["passwdHashed", ALL, md5($passwd)]);
    }

    /**
     * @brief Vérifie si l'association Mot de passe / Identifiant est bon
     * 
     * @Param conn : Connexion à la DB
     * idU : Identifiant utilisateur
     * passwd : Mot de passe
     */
    function isGoodLogin($conn, $idU, $passwd){
        $bddUserRow = getNextRowFrom(getUsers($conn, $idU));
        if($bddUserRow != END && $bddUserRow["passwdHashed"] == md5($passwd)) return true;
        else return false;

    }

?>