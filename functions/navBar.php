
    
<?
include_once "include.php";


function handleDisconnect()
{
    if ($_GET["disconnect"] == "true") {
        $_SESSION["pseudo"] = " ";
        redirectUserTo("index.php");
    }
}

function displayNavBar()
{
    $conn = getLinkToDb();
    $admin = "0";
    $user = getUsers($conn, $_SESSION["pseudo"]);
    if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
        foreach ($user as $use) {
            $admin = $use["admin"];
        }
    }

    echo "<nav class=\"navbar bg-dark d-flex justify-content-around\">";

    echo "<div class=\"col-4\">
            <a href=\"index.php\"><button type=\"button\" class=\"btn btn-outline-light\" style=\"float:left;  margin-right : 5px; margin-bottom:2px\">Accueil</button></a>";
    if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
        if ($admin == "0") {
            echo "<a href=\"My_Photos.php\"><button type=\"button\" class=\"btn btn-outline-light\"  style=\"float:left; margin-right : 5px; margin-bottom:2px\">Mes&#160Photos</button></a>";
        } else if ($admin == "1") {
            echo '<a href="My_Photos.php"><button type="button" class="btn btn-outline-light"  style=\"float:left; margin-right : 5px; margin-bottom:2px\">Toutes&#160les&#160photos</button></a>';
        }
        echo "<a href=\"upload.php\"><button type=\"button\" class=\"btn btn-outline-light\"  style=\"float:left; margin-right :5px;  margin-bottom:2px\">Ajouter&#160une&#160photo</button></a> ";

        if (isIdIn(getLinkToDb(), utilisateurs_db, "idU", $_SESSION["pseudo"], ["admin", "1"])) echo "<a href=\"Statistiques.php\"><button type=\"button\" class=\"btn btn-outline-light\"  style=\"float:left; margin-right : 5px; margin-bottom:2px\">Statistiques&#160du&#160site&#160web</button></a>";
        else echo " ";
    }
    echo '</div>
        
            <form action="index.php" method ="POST">
                <div class="row mx-auto">

            <select name="idCat" class="form-select col" id="idCat">
            <option value=%>Toutes les catégories</option>';

            
            $categories = getCategorie(getLinkToDb());
            foreach ($categories as $row) {
                echo "<option value=\"" . $row["idCat"] . "\">" . $row["nomCat"] . "</option>";
            }
            

    echo '</select>
        <input type="submit" class="form-control col" name="submit" value="Rechercher"></input>
                
            
            
            <div class="col-1\"> <input class="form-control" type="search" name="description" id="description" placeholder="Search" aria-label="Search"></div>
            </div>
            </form>';

    if (!isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
        echo "<div class=\"col-4\">
        <a href=\"Sign_In.php\"><button type=\"button\" class=\"btn btn-outline-light\" style=\"float:right; margin-right : 5px; margin-bottom:2px\">Connexion</button></a>
        <a href=\"Sign_up.php\"><button type=\"button\" class=\"btn btn-outline-light\" style=\"float:right; margin-right : 5px; margin-bottom:2px\">Inscription</button></a>
    </div>";
    } else {
        echo "<div class=\"col-2\">
    <a href=\"modifUser.php\"><button type=\"button\" class=\"btn btn-outline-light\" style=\"float:right; margin-right : 5px; margin-bottom:2px\">" . $_SESSION["pseudo"] . "</button></a>
    <a href=\"?disconnect=true\"><button type=\"button\" class=\"btn btn-outline-light\" style=\"float:right; margin-right : 5px; margin-bottom:2px\"> Deconnexion </button></a>
    </div>";
    }


 if (isIdIn($conn, utilisateurs_db, "idU", $_SESSION["pseudo"])) {
    echo '<script>
    function dateAff(tmp) {
        var diff = {} // Initialisation du retour

        tmp = Math.floor(tmp); // Nombre de secondes entre les 2 dates
        diff.sec = 0
        diff.sec = tmp % 60; // Extraction du nombre de secondes

        tmp = Math.floor((tmp - diff.sec) / 60); // Nombre de minutes (partie entière)
        diff.min = 0
        diff.min = tmp % 60; // Extraction du nombre de minutes

        tmp = Math.floor((tmp - diff.min) / 60); // Nombre dheures (entières)
        diff.hour = 0
        diff.hour = tmp % 24; // Extraction du nombre dheures

        return diff;
    }
    

    function load_message() {
        var heurePage = Date.parse('; echo json_encode($_SESSION["dateCo"]); echo ')
        var now = new Date();
      var diff = (now.getTime() - heurePage)/1000;
      tmp = dateAff(diff)
     console.log(tmp)
     $(\'#seconds\').html(\'<strong>\'+tmp.sec+ \' \' +  \'</strong>\');
      $(\'#minutes\').html(\'<strong>\'+tmp.min+ \' \' +\'</strong>\');
      $(\'#heures\').html(\'<strong>\'+tmp.hour+ \' \' +\'</strong>\');
      \'<\div>\'
    }
    </script>
    ';

    echo '<script> setInterval(\'load_message()\', 1000);</script> <div class = "col-2" style="float:right" > 
    <p style="color:rgb(255,255,255); float:right"> secs </p> 
    <div id = "seconds"  style="color:rgb(255,255,255); float:right;  margin-right : 5px; margin-left : 5px"></div>
    <p style="color:rgb(255,255,255); float:right"> mins</p>
    <div id = "minutes"  style="color:rgb(255,255,255); float:right;  margin-right : 5px; margin-left : 5px"></div>    
    <p style="color:rgb(255,255,255); float:right"> heures </p>
    <div id = "heures" style="color:rgb(255,255,255); float:right; margin-right : 5px; margin-left : 5px"></div>  
    <p style="color:rgb(255,255,255); float:right"> Connexion : </p>
    </div>
    ';
}

    echo "</nav>";
}
?>