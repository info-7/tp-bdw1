<?php
    define('categorie_db', "categorie");
    include_once("bd.php");

    /**
     * @brief Ajoute un image
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     * idU : Identifiant de l'utilisateur dont l'image émane
     * idCat : Identifiant de la catégorie de la photo
     * description : Description de l'image
     * estCachee : Booléeb difinissant si l'image est visible ou non
     */
    function addCategorie($conn, $nomCat){
        return addData($conn, categorie_db, ["nomCat", $nomCat]);
    }

        /**
     * @brief Supprime un image
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     */
    function deleteCategorie($conn, $nomCat){
        return deleteDatas($conn, categorie_db, ["nomCat", $nomCat]);
    }

        /**
     * @brief Retourne les images correspondant à la recherche
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     * admin : Renseigne si l'image est admin ou non
     * passwd : Mot de passe
     */
    function getCategorie($conn, $idCat = ALL, $nomCat = ALL){
        return getDatasLike($conn, categorie_db, ["idCat", $idCat], ["nomCat", $nomCat]);
    }

        /**
     * @brief Met à jour les images
     * 
     * @Param conn : Connexion à la DB
     * nomFich : nom de Fichier
     * admin : Renseigne si l'image est admin ou non
     * passwd : Mot de passe
     */
    function udpateCategorie($conn, $idCat, $nomCat = NO_CHANGE){
        return updateDatas($conn,categorie_db, ["nomCat", ALL, $nomCat], ["idCat", $idCat, NO_CHANGE]);
    }
