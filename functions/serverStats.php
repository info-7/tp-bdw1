<?php
    include_once "include.php";
    define('picturesFolder', "./pictures");
    
    function getSizeOfFolder($f){
        $f = './pictures';
        $io = popen('/usr/bin/du -sk ' . $f, 'r');
        $size = fgets($io, 4096);
        $size = substr($size, 0, strpos($size, "\t"));
        pclose($io);
        return $size/1000 . " Mo";
    }

?>